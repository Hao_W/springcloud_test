package com.knoten.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api("mqtt-core测试")
public class TestController {
    @ApiOperation("测试Feign")
    @GetMapping(value = "/Test")
    public String Test() {
        return "测试成功";
    }

    @ApiOperation("测试Zuul")
    @GetMapping("/TestZuul")
    public String TestZuul() {
        return "this matt-core Zuul";
    }
}
