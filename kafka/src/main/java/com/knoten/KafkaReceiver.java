package com.knoten;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import java.util.Optional;

/**
 * @author wanghao
 * @date 2018/10/31 10:32
 */
@Component
public class KafkaReceiver {
    @KafkaListener(topics = {"Kafka_Test"})
    public void listen(ConsumerRecord<?, ?> record) {
        Optional<?> kafkaMessage = Optional.ofNullable(record.value());
        if (kafkaMessage.isPresent()) {
            Object message = kafkaMessage.get();
            System.err.println("----------------- record =" + record);
            System.err.println("------------------ message =" + message);
        }
    }

}
