package com.knoten;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

/**
 * @author wanghao
 * @date 2018/10/31 10:30
 */
@Component
public class KafkaSend {
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    //发送消息方法
    public void send() {
        Message message = new Message();
        message.setMsg(UUID.randomUUID().toString());
        message.setDate(new Date());
        kafkaTemplate.send("Kafka_Test",message.toString());
    }

}
