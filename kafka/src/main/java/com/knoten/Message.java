package com.knoten;

import java.util.Date;

/**
 * @author wanghao
 * @date 2018/10/31 10:29
 */
public class Message {
    private String msg;
    private Date date;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Message{" +
                "msg='" + msg + '\'' +
                ", date=" + date +
                '}';
    }
}
