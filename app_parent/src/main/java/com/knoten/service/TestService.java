package com.knoten.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@FeignClient(name="MQTT-CORE")
public interface TestService {
    @RequestMapping(value = "/Test",method = RequestMethod.GET)
    public String Test();
}
