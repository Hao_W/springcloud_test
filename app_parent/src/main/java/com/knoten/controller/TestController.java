package com.knoten.controller;

import com.knoten.model.User;
import com.knoten.service.TestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api("app-parent的测试控制器")
public class TestController {

    @Autowired
    private TestService testService;

    @ApiOperation("测试Feign")
    @PostMapping("/index")
    public String Test(@RequestBody User user) {
        System.out.println(user.toString());
        return testService.Test();
    }

    @ApiOperation("测试路由")
    @GetMapping("/TestZuul")
    public String TestZuul() {
        return "this app-parent Zuul";
    }
}
