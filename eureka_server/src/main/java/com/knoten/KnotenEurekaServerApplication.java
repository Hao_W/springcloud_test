package com.knoten;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class KnotenEurekaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(KnotenEurekaServerApplication.class, args);

    }
}
