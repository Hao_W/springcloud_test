package com.knoten.controller;

import com.knoten.config.TestConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestConfigController {
    @Autowired
    private TestConfig testConfig;
    @RequestMapping("/TestConfig")
    public String Test(){
        return testConfig.toString();
    }
}
